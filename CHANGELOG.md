## [2.4.8](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.4.7...v2.4.8) (2024-11-28)


### Bug Fixes

* Add `retry: 2` to `container-sbom` and `container-scanning` job due to a GitLab limit that only allows max 2 retries. ([15d57cc](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/15d57ccc171405eacad226cb1c87c885f16cb695))

## [2.4.7](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.4.6...v2.4.7) (2024-11-28)


### Bug Fixes

* Add `retry: 3` to `container-sbom` and `container-scanning` job due to the upstream database pull limits. ([7b2dfad](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/7b2dfad6fa5421818845339f89d25906431170dc))

## [2.4.6](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.4.5...v2.4.6) (2024-11-11)


### Bug Fixes

* Update registry and images. ([58bcf78](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/58bcf78545a8c734f1ac087def86b61dd55b1b96))

## [2.4.5](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.4.4...v2.4.5) (2024-11-11)


### Bug Fixes

* Update registry and images. ([c71a22b](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/c71a22b6f6ca1a0a490200a5ca940bb5c0355f38))

## [2.4.4](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.4.3...v2.4.4) (2024-11-11)


### Bug Fixes

* **container:** Add mirror for trivy java database ([66547ed](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/66547ed171ad216ce099e357823b0a5b0807f6e2))

## [2.4.3](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.4.2...v2.4.3) (2024-11-05)


### Bug Fixes

* Switch to OpenCoDE hosted ClamAV database. ([2556064](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/25560646ba5e2b0513639f14dfad43004c1e7f01))

## [2.4.2](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.4.1...v2.4.2) (2024-10-08)


### Bug Fixes

* **ci:** Add db-repository mirror for trivy ([75dbd45](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/75dbd45cc645def6b7b0f868b4dbced85a37bdbc))

## [2.4.1](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.4.0...v2.4.1) (2024-10-08)


### Bug Fixes

* **ci:** Add db-repository mirror for trivy ([9007035](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/900703544b0c2f58a43538c033d4f5d314d73edd))

# [2.4.0](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.3.5...v2.4.0) (2024-10-03)


### Features

* **images:** Update all container images to latest and remove souvap-univention.de registry ([88436ed](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/88436ed9edcc625739c662b4d6e3edce894a5bf4))

## [2.3.5](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.3.4...v2.3.5) (2024-10-03)


### Bug Fixes

* Remove souvap-univention.de for ClamAV sync and update paths in README.md. ([d74ccdc](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/d74ccdcbefcb5a0e0dbba8fe07f9cd7b0356b7f9))

## [2.3.4](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.3.3...v2.3.4) (2024-09-23)


### Bug Fixes

* Update reuse image to 4.0.3. ([70e5034](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/70e50343e10a1e08fa3a1498dbe6244c989cd3b3))

## [2.3.3](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.3.2...v2.3.3) (2024-05-30)


### Bug Fixes

* **ci:** Fix regex variable in common/lint.yml ([e65b369](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/e65b369061f33aeff87662be04f8c74abe198e0c))

## [2.3.2](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.3.1...v2.3.2) (2024-02-16)


### Bug Fixes

* **ci:** Create MERGE_REQUEST_BOT_TARGET_BRANCH for change target branch in automr ([b138907](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/b1389076594996a61a6904279bc1a7c580801c7b))

## [2.3.1](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/compare/v2.3.0...v2.3.1) (2023-12-28)


### Bug Fixes

* **ci:** Increate timeout of trivy and disable secrets scanning ([fcd5dfb](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config/commit/fcd5dfb95107c65d6afdde0a44dbf3b5d38ca111))

# [2.3.0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v2.2.0...v2.3.0) (2023-12-27)


### Features

* **ci:** Create modular container build configuration ([4316abc](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/4316abce2e04821452e52d1e4a41a55a385c58e8))

# [2.2.0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v2.1.4...v2.2.0) (2023-12-27)


### Features

* **ci:** Allow customization of BUILD_CONTEXT and DOCKERFILE for docker-build ([4814351](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/4814351a49acf4185ed213cd289d69203fc94125))

## [2.1.4](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v2.1.3...v2.1.4) (2023-12-21)


### Bug Fixes

* **ci:** Replace syft by trivy ([5d9cade](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/5d9cadef7947aab7e0e737a8abfc91fed5d69d2c))

## [2.1.3](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v2.1.2...v2.1.3) (2023-12-15)


### Bug Fixes

* **ci:** Add login to crane and trivy ([90a09ba](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/90a09ba5d8a41eaed034a8b5b89f29364d4e22a4))

## [2.1.2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v2.1.1...v2.1.2) (2023-12-14)


### Bug Fixes

* **ci:** Skip cosign spdx uploads when no attestation is done in pipeline ([93a2fa5](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/93a2fa50416df688a55f237b02544cb06fee18a3))

## [2.1.1](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v2.1.0...v2.1.1) (2023-12-14)


### Bug Fixes

* **ci:** Switch condition in dockerconfigjson ([6b4b7c5](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/6b4b7c5a86f2e4e02e5545d17814906ddc1cad44))

# [2.1.0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v2.0.0...v2.1.0) (2023-12-14)


### Features

* **ci:** Add ADDITIONAL_ARGS flag to allow additional commandline arguments for kaniko ([d72a4b8](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/d72a4b8beabb715b01b566163a283097abeb9a3b))

# [2.0.0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.4.0...v2.0.0) (2023-12-13)


### Features

* **ci:** Add ClamAV scanning ([faaaebc](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/faaaebc40bcd4dabea54777389a8ab0e48116d6a))


### BREAKING CHANGES

* **ci:** Split container build into separate stages

# [1.4.0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.3.2...v1.4.0) (2023-12-06)


### Bug Fixes

* **ci:** Only push single snapshot and add default labels ([3cea21c](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/3cea21c2bb0e339ced654a0c66521a8e34788c00))
* **docs:** Add conventional-commits and docker lint. ([43978d4](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/43978d48a46be7762300518b89013c1b77d2e051))


### Features

* **ci:** Add conventional commit checker to merge-requests ([103a658](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/103a658537ba31c94f0cb661bba541c625588e19))
* **ci:** Add Hadolint as container linter and update all images ([9eb063e](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/9eb063eba9bee746eb68781e2a984280f9e869d2))

## [1.3.2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.3.1...v1.3.2) (2023-11-28)


### Bug Fixes

* **ci:** Rename WIP to Draft ([ec0a12f](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/ec0a12f61a0b0283c5d5680f2f6e6dc3c8b2e70b))

## [1.3.1](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.3.0...v1.3.1) (2023-11-27)


### Bug Fixes

* **ci:** Update token with extended permissions ([b98c125](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/b98c125f8b343dad89c6bda001df808ecc61f29a))

# [1.3.0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.2.0...v1.3.0) (2023-11-27)


### Features

* **ci:** Open automatic MRs for new branches ([1de5181](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/1de5181bb350181a9ad67745dd2952a45b912e2c))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.9...v1.2.0) (2023-09-20)


### Bug Fixes

* **ci:** Use cache for digest cause kaninko image flaky ([2cdab42](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/2cdab42d1f137273aea384027ebae1b287274baf))


### Features

* **ci:** Add container-build-semantic to use semantic versioning for container builds ([1c1cb1c](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/1c1cb1cc2aaaca04db1937c05d24feac3a3fac4e))

## [1.1.9](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.8...v1.1.9) (2023-09-19)


### Bug Fixes

* **ci:** Rollback and remove after_script ([c5e2255](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/c5e225523f3d18fd787a6f2b1829e0e48bd2dacf))
* **ci:** Use after_script for container build digest ([af4c8c2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/af4c8c28271d5dde014b9d37117a34bae18d4b21))

## [1.1.8](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.7...v1.1.8) (2023-09-18)


### Bug Fixes

* **ci:** Suffix all images with branch-slug when not on default branch ([8ca1c84](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/8ca1c84f34cdd1f96951527ff6a78774462a4aae))

## [1.1.7](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.6...v1.1.7) (2023-09-06)


### Bug Fixes

* **ci:** Publish container images on branches with commit slug ([9464c18](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/9464c18e023291dff4936a0f09bead8bcc692569))

## [1.1.6](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.5...v1.1.6) (2023-08-30)


### Bug Fixes

* **ci:** Disable merge_request_events ([a662740](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/a6627406e342c140ec2254df8348b1aa7f0f3ea2))
* **ci:** Fix regex in workflow ([0796bd2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/0796bd24adf66e553513fd000b9b293b40d4cd15))

## [1.1.5](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.4...v1.1.5) (2023-08-29)


### Bug Fixes

* **ci:** Replace except/only with rules in semantic-release.yml ([07e438b](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/07e438bf77f639753b73b388ff14b63df078e3a1))
* **ci:** Update conditions in rules ([ee4f1fc](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/ee4f1fca77fedf446bd05d13f6d37054b00270c1))

## [1.1.4](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.3...v1.1.4) (2023-08-16)


### Bug Fixes

* **helm:** Allow access to RELEASE_VERSION in docs envsubst step ([1123bc7](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/1123bc7e1a95ab61a2c8d19cfdcbbfd161465a8f))

## [1.1.3](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.2...v1.1.3) (2023-08-15)


### Bug Fixes

* **helm:** Fetch prov files explicit from cache ([1217a6f](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/1217a6fcb135fed2f1135d2743bff50534d0b2ff))
* **release-automation:** Remove leading v char when using git tag ([573d173](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/573d173cf528985e212c4977987ef9a123b1c605))

## [1.1.2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.1...v1.1.2) (2023-08-15)


### Bug Fixes

* **helm:** Provide registry for login ([6603ed3](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/6603ed3a744b1f77135ac4ea2c21f13cffd5ecbc))

## [1.1.1](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.1.0...v1.1.1) (2023-08-15)


### Bug Fixes

* **helm:** Add registry login for oci image ([ba03be8](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/ba03be8d6f3fd8f229f5ad46a6f4d6ad74024f74))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.0.2...v1.1.0) (2023-08-15)


### Features

* **helm:** Add OCI push ([ba0ae2d](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/ba0ae2d216318fe3c5663cc858e0d45681e5b89a))

## [1.0.2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.0.1...v1.0.2) (2023-08-08)


### Bug Fixes

* **ci:** Change release commit user to Pipeline user ([12541a6](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/12541a6e11650fffc2c6e91443b77e93285d32eb))

## [1.0.1](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/compare/v1.0.0...v1.0.1) (2023-08-07)


### Bug Fixes

* **helm:** Use SPDX instead of CycloneDX format ([eaa511e](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/eaa511ea70cee2f345744f8f7433198dac3ca511))

# 1.0.0 (2023-08-07)


### Bug Fixes

* **ci:** Remove changelog file for reuse ([227b240](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/227b240e8d920e0633fb843d645127427f903bca))
* **ci:** Remove path in reuse command ([70ee81b](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/70ee81bc0ecd9d1e0adbf5612bc6e6e5b1c6910b))
* **common:** add ignore for chart templates for yamllint ([6626917](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/6626917fcdd8e904237857be54608ee740b127ed))
* **common:** optimize yaml linter ([d1b4af2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/d1b4af28d0f0e727d36ea0dcee11c388f6791dfd))
* **helm:** add except for for triggers ([3d1bc26](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/3d1bc268c247a8e3721bc3116ca65f82d7a4961e))
* **helm:** add extend to helm-dependency-build ([c26b9dc](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/c26b9dcedc6274dd0b5a84691510c0fecd9b92a5))
* **helm:** add image to dependency-build ([a8e0689](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/a8e0689caf23fdd0b499d8421e5dfa11694d8256))
* **helm:** add path to caches ([4944e77](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/4944e77e2809680031ed4148c0e5f50cd0822e8c))
* **helm:** add release pipeline ([0defc25](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/0defc25c31baab90a770e95ddd18474e617c6a23))
* **helm:** allow failure in lint step ([3d5de38](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/3d5de381c689bfb6f798752356eadfd2c49781ff))
* **helm:** fail when there is no release version ([6dbdd12](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/6dbdd12b727a4de0ab0f7a5aca765f80c991f93d))
* **helm:** fix helm lint script command ([3470622](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/347062209356384364576a3377c334293d1777b1))
* **helm:** fix issue with multiple charts while generating docs ([85ddbf4](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/85ddbf4bd16ebf29784473554b62afb2466f4d71))
* **helm:** fix needs of helm-package ([cf66894](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/cf66894c498d4a0886ad3611852b944d785b179c))
* **helm:** fix path to caches ([6f84809](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/6f84809f3c3f1faa1b4ca5a76616c5a9e73bb68e))
* **helm:** fix path to chart in lint step ([056d893](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/056d89307f71d07e44e70146a8e3c5db5b9b863f))
* **helm:** Generalize variable and add docs ([90bd2b4](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/90bd2b400dfed5acbe89d28f58364abb7479d07f))
* **helm:** lint iterate over all charts ([041ebbb](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/041ebbbf7ba1d7b0e9d918080d91f81856391243))
* **helm:** lint use full path ([974c54a](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/974c54a6aa42fffc1a981b102a63841b8f2ce314))
* **helm:** only upload package tgz and not provs ([6cc6874](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/6cc6874df6f94107910891d2a4d09d7fecada844))
* **helm:** replace environment variables via envsubst ([ab2a93c](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/ab2a93ce997aa845542966acab38a0d3fe6aed77))
* **release-automation:** also release on web ([63f8955](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/63f8955c0cf94185a63b3ad100aa510277fdad36))
* **release-automation:** fix generate-release-version ([b1efabc](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/b1efabcf87b3506b6a50afb11943cdd0beeae958))
* **release-automation:** fix missing quote on generate-release-version ([b36fe04](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/b36fe04fd4266691a152693f1b0f33ae0e9f0e9d))
* **release-automation:** fix releaserc substition ([f23baf2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/f23baf2b1e91ce1d655ed5d6dfb03e8b82109050))
* **release-automation:** let plugins in single line ([2896de2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/2896de2bcdaa70e75c62ea1f50bc5c2913dee45f))
* **release-automation:** use a releaserc and commit releases ([9d3995d](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/9d3995d76ece6b26005bcc52fac849a674a5f1f1))


### Features

* **ci:** Add reuse to linting ([9a0ace2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/9a0ace22aa0a3021b7402a9303659faa99c2a98c))
* **ci:** Initial commit of ci folder ([0ec9fc0](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/0ec9fc0e94c572c74b2bcdf91521d053826e7683))
* **common:** add yamllinter ([6533dd5](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/6533dd51f06438cff03addf4664377f83ee9d0a0))
* **container:** add container build ([68a20b9](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/68a20b93d70f784a554a65a9a81753bead41e9ea))
* **container:** Add SBOM and container scanning ([f7df47f](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/f7df47fa057f55dd4b7c45b85e9184edfdbb9a4b))
* **helm:** add caching ([bfbbb2f](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/bfbbb2f3ad3a90322ede0f3f6d3db9af25d8d9ec))
* **helm:** Add helm docs generation ([9735340](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/9735340cd0eeea6b82e43cac734a3c3ea7ed15bd))
* **helm:** add helm lint ([7fc8238](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/7fc82387edeacf9a5f3e784d97bcee9201f1f28b))
* **helm:** Add prereleases for helm ([1fb9bef](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/1fb9bef9c44e8ec1598fdc53ad85ba849b0997c5))
* **helm:** fix helm lint image ([972bcb2](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/972bcb21cb8e691df95588f41a1c99197052b0e7))
* **helm:** sign charts with gpg keys ([777d6c6](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config/commit/777d6c6ace619137aa0c04c84a9dea6562e21e05))
